#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void lecture(char * nom_fichier);
//bool ligne_nourriture(std::string line);
void decodage_ligne_fourmi(std::string line);

int main(int argc, char * argv[])
{
	if(argc != 2);
	
	lecture(argv[1]);

	return EXIT_SUCCESS;
}

void lecture(char * nom_fichier)
{
    string line;
    ifstream fichier(nom_fichier); 
    if(!fichier.fail()) 
    {
        // l’appel de getline filtre aussi les séparateurs
        while(getline(fichier >> ws,line)) 
        {
			// ligne de commentaire à ignorer, on passe à la suivante
			if(line[0]=='#')  continue;  
       
			decodage_ligne_fourmi(line);
        }
        cout << "fin de la lecture" << endl;
	}
	else;
}

//bool ligne_nourriture(std::string line)
//{
	//enum Etat_lecture {NB, NOURRITURE};
	
	//istringstream data(line);
	//static int etat(NB);
	//static int i(0), total(0);
	//int x(0), y(0);

	//switch(etat) 
	//{
	//case NB: 
		//if(!(data >> total)) exit(0); 
		//else i = 0;
		//if(total == 0) 
		//{
			//cout << "Pas de nourriture" << endl;
			//return true; 
		//}
		//else etat=NOURRITURE ; 
		//cout << "Nb de nourriture: " << total << endl; 
	    //break;

	//case NOURRITURE: 
		//if(!(data >> x >> y)) exit(0);
		//else ++i;
		//cout << "Nourriture " << i << ": ("  << x << "," << y << ")" << endl;
		//if(i == total) return true;
	    //break;
	//}
	//return false;
//}

void decodage_ligne_fourmi(std::string line)
{
	enum Etat_lecture {NBC, COLLECTOR, NBD, DEFENSOR, NBP, PREDATOR};
	static int etat(NBC);
	std::istringstream data(line);
	static int iC(0), iD(0), iP(0), nbC(0), nbD(0), nbP(0);
	int x(0), y(0), age(0), food(0);
	
	switch(etat) 
	{
	case NBC: 
		if(!(data >> nbC)) exit(EXIT_FAILURE); 
		else iC = 0;
		if(nbC == 0) 
		{
			std::cout << "Pas de collector" << std::endl; 
			etat = NBD;
		}
		else etat=COLLECTOR ; 
		std::cout << "Nb de collector(s): " << nbC << std::endl; 
	    break;
	    
	case COLLECTOR:
		break;
		if(!(data >> x >> y >> age >> food))
			exit(EXIT_FAILURE);
		else ++iC;
		if(iC == nbC) etat=NBD;
		std::cout << "Collector " << iC << ": "  
		<< x << " " 
		<< y << " "
		<< age << " "
		<< food << " " << std::endl;
	case NBD: 
		if(!(data >> nbD)) exit(EXIT_FAILURE); 
		else iD = 0;
		if(nbD == 0) 
		{
			std::cout << "Pas de defensor" << std::endl;
			etat = NBP;
		}
		else etat=DEFENSOR ; 
		std::cout << "Nb de defensor(s): " << nbD << std::endl; 
	    break;
	    
	case DEFENSOR:
		if(!(data >> x >> y >> age))
			exit(EXIT_FAILURE);
		else ++iD;
		if(iD == nbD) etat=NBP;
		std::cout << "Defensor " << iD << ": "  
		<< x << " " 
		<< y << " "
		<< age << " " << std::endl;
		break;   
	
	case NBP: 
		if(!(data >> nbP)) exit(EXIT_FAILURE); 
		else iP = 0;
		if(nbP == 0) 
		{
			std::cout << "Pas de predator" << std::endl;
		} 
		else etat=PREDATOR ; 
		std::cout << "Nb de predator(s): " << nbP << std::endl; 
	    break;

	case PREDATOR:
		if(!(data >> x >> y >> age))
			exit(EXIT_FAILURE);
		else ++iP;
		if(iP == nbP); //return true; --> passage prochaine fourmiliere
		std::cout << "Predator " << iP << ": "  
		<< x << " " 
		<< y << " "
		<< age << " " << std::endl;
		break; 
	}
}
